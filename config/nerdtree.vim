" NERDTree is only loaded when NERDTreeToggle command is pressed
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' } 
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeToggle' }
