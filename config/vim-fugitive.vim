" Gdiffsplit/Gvdiffsplit, which is provided by vim-fugitive is a brilliant tool for resolving git confilit merging.
" It will bring up 3 windows(target[HEAD]/working/merge), and all you need is
" navigating with `[c/]c` then use the `diffget/diffput` command to retain
" what you want.
"   `:only` closs all windows apart from the current one.
Plug 'tpope/vim-fugitive'
