" With this plugin, you can use most mappings in a complementary pairs way.
"   `]q` :cnext `[q` :cprevious
"   `]Q` :clast `[Q` :cfirst
"   `]b` :bnext `[b` :bprevious
"   `]B` :blast `[B` :bfirst
"   `]a` :next  `[a` :prev
"   `]l` :lnext `[l` :lprevious
"   `]L` :llast `[L` :lfirst
"   `]t` :tnext `[t` :tprevious
"   `]T` :tlast `[t` :tfirst
"   `]<C-L>` :lnfile `[<C-L>` :lpfile
" References @unimpaired.txt for more details.
Plug 'tpope/vim-unimpaired'
