Plug 'preservim/tagbar', {'for': ['c', 'cpp', 'go', 'python', 'sh', 'javascript']}
" Sort tags by their order in the source file. Press 's' to sort them
" alphabetically.
let g:tagbar_sort = 0
" Automatically moved to tagbar window while it is opened.
let g:tagbar_autofocus = 1

